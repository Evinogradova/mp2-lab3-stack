##Методы программирования 2: Стек

###Цели и задачи

*Цель данной работы* -- разработка двух видов стеков:
- простейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.

Перед выполнением работы был получен проект-шаблон, содержащий:
- Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
- Тестовый пример использования класса `TStack`

Выполнение работы предполагает решение следующих задач:
1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
2. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
3. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
4. Разработка тестов для проверки работоспособности стеков.
5. Реализация алгоритма проверки правильности введенного арифметического выражения.
6. Реализация алгоритмов разбора и вычисления арифметического выражения.
7. Обеспечение работоспособности тестов и примера использования.

###Разработка стеков

#####TSimpleStack на основе массива фиксированной длины

```cpp
#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define DefMemSize 20

class TSimpleStack
{
private:
	int top;
	int data[DefMemSize];
public:
	TSimpleStack() { top = 0; }
	TSimpleStack(const TSimpleStack &st)
	{
		top = st.top;
		for (int i = 0; i < top; i++)
			data[i] = st.data[i];
	}
	bool IsFull() const { return top == DefMemSize; }
	bool IsEmpty() const { return top == 0; }
	void Put(const int val)
	{
		if (IsFull())
			throw "Stack is full";
		data[top++] = val;
	}
	int Get()
	{
		if (IsEmpty())
			throw "Stack is empty";
		return data[--top];
	}
};

#endif
```

#####Реализация методов класса TDataRoot согласно заданному интерфейсу

```cpp
#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	DataCount = 0;
	MemSize = Size;
	if (Size < 0)
		throw SetRetCode(DataErr);
	else if (Size == 0)
	{
		pMem = nullptr;
		MemType = MEM_RENTER;
	}
	else
	{
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}
}

TDataRoot::~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = nullptr;
}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	for (int i = 0; i < DataCount; i++)
		((PTElem)p)[i] = pMem[i];
	if (MemType == MEM_HOLDER)
		delete[]pMem;
	MemType = MEM_RENTER;
	pMem = (PTElem)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}
```

#####Класс TStack

######Объявление:
```cpp
#ifndef __STACK_H__
#define __STACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
protected:
	int Top;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Top = -1; }
	TStack(const TStack &);
	void Put(const TData &);
	TData Get();
	int  IsValid();
	void Print();
	TData GetTopElem();
};

#endif
```
######Реализация:
```cpp
#include "tstack.h"
#include <iostream>

using namespace std;

TStack::TStack(const TStack &st)
{
	DataCount = st.DataCount;
	MemSize = st.MemSize;
	MemType = st.MemType;
	pMem = new TElem[MemSize];
	for (int i = 0; i < DataCount; i++)
		pMem[i] = st.pMem[i];
}

void TStack::Put(const TData &Val)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else
	{
		if (IsFull())
		{
			void* p = new TElem[MemSize + DefMemSize];
			SetMem(p, MemSize + DefMemSize);
		}
		pMem[++Top] = Val;
		DataCount++;
	}
}

TData TStack::Get()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else
	{
		DataCount--;
		return pMem[Top--];
	}
}

int  TStack::IsValid()
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}

void TStack::Print()
{
	if (DataCount == 0)
		cout << "Stack is empty!";
	for (int i = 0; i < DataCount; ++i)
		cout << pMem[i] << " ";
	cout << endl;
}

TData TStack::GetTopElem()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else
		return pMem[Top];
}
```

#####Тесты, разработанные для проверки работоспособности стеков

######Для TSimpleStack:
```cpp
#include "tsimplestack.h"

#include <gtest/gtest.h>

TEST(TSimplestack, can_creata_stack)
{
	ASSERT_NO_THROW(TSimpleStack st());
}

TEST(TSimplestack, new_stack_is_empty)
{
	TSimpleStack st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimplestack, stack_with_element_not_empty)
{
	TSimpleStack st;
	st.Put(1);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimplestack, stack_after_get_operation_not_full)
{
	TSimpleStack st;
	for (int i = 0; i < DefMemSize; i++)
		st.Put(i);

	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TSimplestack, get_operation_take_top_element)
{
	TSimpleStack st;
	st.Put(0);
	st.Put(1);
	st.Put(2);

	EXPECT_EQ(2, st.Get());
}

TEST(TSimplestack, throws_when_put_in_full_stack)
{
	TSimpleStack st;
	for (int i = 0; i < DefMemSize; i++)
		st.Put(i);

	ASSERT_ANY_THROW(st.Put(DefMemSize));
}

TEST(TSimplestack, throws_when_get_from_empty_stack)
{
	TSimpleStack st;

	ASSERT_ANY_THROW(st.Get());
}

TEST(TSimplestack, can_create_copied_stack)
{
	TSimpleStack st;

	ASSERT_NO_THROW(TSimpleStack st1(st));
}

TEST(TSimplestack, copied_stack_is_equal_to_source_one)
{
	bool res = true;
	TSimpleStack st1;
	for (int i = 0; i < DefMemSize; i++)
		st1.Put(i);
	TSimpleStack st2(st1);
	for (int i = 0; i < DefMemSize; i++)
		if (st1.Get() != st2.Get())
			res = false;

	EXPECT_TRUE(res);
}

TEST(TSimplestack, copied_stack_has_its_own_memory)
{
	TSimpleStack st1;
	TSimpleStack st2(st1);

	EXPECT_NE(&st1, &st2);
}
```
######Для TStack:
```cpp
#include "tstack.h"

#include <gtest/gtest.h>

TEST(TStack, can_create_stack)
{
	ASSERT_NO_THROW(TStack st());
}


TEST(TStack, new_stack_is_empty)
{
	TStack st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, stack_with_element_not_empty)
{
	TStack st;
	st.Put(1);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, stack_after_get_operation_not_full)
{
	TStack st(2);
	st.Put(0);
	st.Put(1);
	st.Put(2);

	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, get_operation_take_top_element)
{
	TStack st(2);
	st.Put(0);
	st.Put(1);
	st.Put(2);

	EXPECT_EQ(2, st.Get());
}

TEST(TStack, throws_when_get_from_empty_stack)
{
	TStack st(2);

	ASSERT_ANY_THROW(st.Get());
}

TEST(TStack, can_create_copied_stack)
{
	TStack st(5);

	ASSERT_NO_THROW(TStack st1(st));
}

TEST(TStack, copied_stack_is_equal_to_source_one)
{
	const int size = 3;
	bool res = true;
	TStack st1(size);
	for (int i = 0; i < size; i++)
		st1.Put(1);
	TStack st2(st1);
	for (int i = 0; i < size; i++)
	if (st1.GetTopElem() != st2.GetTopElem())
		res = false;

	EXPECT_TRUE(res);
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	const int size = 3;
	TStack st1(size);
	TStack st2(st1);

	EXPECT_NE(&st1, &st2);
}

TEST(TStack, can_put_in_full_stack)
{
	const int size = 3;
	TStack st(size);
	for (int i = 0; i < size; i++)
		st.Put(1);

	ASSERT_NO_THROW(st.Put(1));
}

TEST(TStack, gettopelem_show_top_element)
{
	const int size = 3;
	TStack st(size);
	for (int i = 0; i < size; i++)
		st.Put(1);

	EXPECT_EQ(1, st.GetTopElem());
}

TEST(TStack, gettopelem_not_take_element_from_stack)
{
	const int size = 3;
	TStack st(size);
	for (int i = 0; i < size; i++)
		st.Put(1);

	st.GetTopElem();

	EXPECT_TRUE(st.IsFull());
}
```
######Подтверждение успешного прохождения тестов:
![Test_stack.png](http://s020.radikal.ru/i706/1612/9a/40e2243ec012.png)

###Вычисление арифметических выражений

#####Понятие арифметического выражения

Арифметическое выражение - выражение, в котором операндами являются объекты, над которыми выполняются арифметические операции. Например,
`(1+2)/(3+4*6.7)-5.3*4.4`
При такой форме записи (называемой инфиксной, где знаки операций стоят между операндами) порядок действий определяется расстановкой скобок и приоритетом операций. Постфиксная (или обратная польская) форма записи не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:
`1 2+ 3 4 6.7*+/ 5.3 4.4* -`

#####Контроль над расстановкой скобок

Требуется проанализировать соответствие открывающих и закрывающих круглых скобок во введённом арифметическом выражении. Программа должна напечатать таблицу соответствия скобок, причем в таблице должно быть указано, для каких скобок отсутствуют парные им, а также общее количество найденных ошибок. Для идентификации скобок могут быть использованы их порядковые номера в выражении.

#####Алгоритм решения
######Проверка скобок
На вход алгоритма поступает строка символов, на выходе должна быть выдана таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.
- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

######Перевод в постфиксную форму

Данный алгоритм основан на использовании стека. На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой. Каждой операции и скобкам приписывается приоритет.

- ( - 0

- ) - 1

- +- - 2

- */ - 3

Предполагается, что входная строка содержит синтаксически правильное выражение.
Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:
- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек. Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают. После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.

######Вычисление

Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком
Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.

#####Реализация функций, вычисляющих арифметическое выражение

######Объявление функций
```cpp
#include "tstack.h"
#include <iostream>
#include <string>

using namespace std;

bool ExpressionControl(const string& IncomStr);  //Проверка на недопустимые символы
bool BracketsControl(const string& IncomStr);    // Проверка расстановки скобок
int OperatorPriority(char);                      // Присвиваем приоритет операциям
bool IsOperator(char);
string PostfixForm(const string& IncomStr);      // Перевод в постфиксную форму
TData ExpressionRes(const string& PostfixStr);   // Вычисление по постфиксной форме
TData Calculate(const string& IncomStr);

```
######Реализация
```cpp
#include "arithmetic_expression.h"

bool ExpressionControl(const string& IncomStr)
{
	for (string::const_iterator it = IncomStr.begin(); it != IncomStr.end(); ++it)
		if (*it < '(' || *it > '9')
			return false;
	return true;
}

bool BracketsControl(const string& IncomStr)
{
	TStack Brackets;
	int Err = 0, BrIndex = 0;
	bool result = true;
	if (!ExpressionControl(IncomStr))
		throw "Incorrect experssion";
	for (string::const_iterator it = IncomStr.begin(); it != IncomStr.end(); ++it)
	{
		if (*it == '(')
		{
			BrIndex++;
			Brackets.Put((TData)BrIndex);
		}
		if (*it == ')')
		{
			BrIndex++;
			if (Brackets.IsEmpty())
			{
				Err++;
				cout << "-  " << BrIndex << endl;
			}
			else
				cout << Brackets.Get() << "  " << BrIndex << endl;
		}
	}
	while (!Brackets.IsEmpty())
	{
		cout << Brackets.Get() << "  -" << endl;
		Err++;
	}
	if (Err > 0)
		result = false;
	return result;
}

int OperatorPriority(char Op)
{
	switch (Op)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	default: return -1;
	}
}

bool IsOperator(char IncomSymb)
{
	if (IncomSymb == '(' || IncomSymb == ')' || IncomSymb == '+' || IncomSymb == '-' || IncomSymb == '*' || IncomSymb == '/')
		return true;
	else
		return false;
}

string PostfixForm(const string& IncomStr)
{
	string PostfixStr;
	TStack Op;
	if (!BracketsControl(IncomStr))
		throw "Incorrect arithmetic expression";
	for (string::const_iterator it = IncomStr.begin(); it != IncomStr.end(); ++it)
	{
		if (!IsOperator(*it))
			PostfixStr.push_back(*it);
		else
		{
			PostfixStr.push_back(' ');
			if (OperatorPriority(*it) == 0 || Op.IsEmpty() || OperatorPriority(*it) > OperatorPriority(Op.GetTopElem()))
				Op.Put(*it);
			else if (*it == ')')
			{
				while (Op.GetTopElem() != '(')
					PostfixStr.push_back((char)Op.Get());
				Op.Get();
			}
			else
			{
				while (!Op.IsEmpty() && OperatorPriority(*it) <= OperatorPriority(Op.GetTopElem()))
					PostfixStr.push_back((char)Op.Get());
				Op.Put(*it);
			}
		}
	}
	PostfixStr.push_back(' ');
	while (!Op.IsEmpty())
		PostfixStr.push_back((char)Op.Get());
	return PostfixStr;
}

TData ExpressionRes(const string& PostfixStr)
{
	TStack Num;
	string tmp;
	TData num1 = 0, num2 = 0;
	for (string::const_iterator it = PostfixStr.begin(); it != PostfixStr.end(); ++it)
	{
		if (IsOperator(*it))
		{
			num2 = Num.Get();
			num1 = Num.Get();
			if (*it == '+')
				Num.Put(num1 + num2);
			else if (*it == '-')
				Num.Put(num1 - num2);
			else if (*it == '*')
				Num.Put(num1 * num2);
			else if (*it == '/')
				Num.Put(num1 / num2);
		}
		else if (*it != ' ')
			tmp.push_back(*it);
		else if (*it == ' ' && tmp != "")
		{
			Num.Put(atof(tmp.c_str()));
			tmp = "";
		}
	}
	return Num.Get();
}

TData Calculate(const string& IncomStr)
{
	return ExpressionRes(PostfixForm(IncomStr));
}
```
######Тесты
```cpp
#include "arithmetic_expression.h"

#include <gtest/gtest.h>

TEST(arithmetic_expression, can_control_if_incom_string_correct)
{
	EXPECT_FALSE(ExpressionControl("(1+2')-3"));
}

TEST(arithmetic_expression, can_control_brackets)
{
	EXPECT_TRUE(BracketsControl("(1+2)-3"));
}

TEST(arithmetic_expression, can_control_missed_brackets)
{
	EXPECT_FALSE(BracketsControl("(3-1"));
}

TEST(arithmetic_expression, can_control_many_brackets)
{
	EXPECT_TRUE(BracketsControl("((3+2)*8+(4-1))/(15-4)"));
}

TEST(arithmetic_expression, can_turn_to_postfix_form)
{
	EXPECT_EQ(" 1 2 +  4 3 - /", PostfixForm("(1+2)/(4-3)"));
}

TEST(arithmetic_expression, can_calculate_postfix_form)
{
	EXPECT_EQ(3, ExpressionRes(PostfixForm("(1+2)/(4-3)")));
}

TEST(arithmetic_expression, can_calculate_large_expression)
{
	EXPECT_EQ((1 + 2) / (3 + 4 * 6.7) - 5.3 * 4.4, ExpressionRes(PostfixForm("(1+2)/(3+4*6.7)-5.3*4.4")));
}
```
![Test_sample.png](http://s020.radikal.ru/i700/1612/e7/439d632db905.png)

#####Пример использования
```cpp
#include <iostream>
#include "tstack.h"
#include "arithmetic_expression.h"

using namespace std;

void main()
{
	string str;
	TData result;

	cout << "Enter arithmetic expression:" << endl;
	cin >> str;
	cout << "Brackets control" << endl;
	result = Calculate(str);
	cout << "Answer:" << result << endl;
}
```
![Sample.png](http://s013.radikal.ru/i324/1612/bd/7d78070b5b37.png)

###Вывод

В данной работе были реализованы классы для двух видов стека: `TSimpleStack` - простой стек, основанный на статическом массиве, и `TStack` - более сложный, основанный на использовании динамической структуры и являющийся производным классом от `TDataRoot`.

Также были разработаны функции, позволяющие вычислять арифметические выражения.

Тесты, написанные не только для классов `TSimpleStack` и `TStack`, но и для функций вычисления арифметического выражения, помогли найти и исправить большую часть ошибок.