#include "tsimplestack.h"

#include <gtest/gtest.h>

TEST(TSimplestack, can_creata_stack)
{
	ASSERT_NO_THROW(TSimpleStack st());
}

TEST(TSimplestack, new_stack_is_empty)
{
	TSimpleStack st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimplestack, stack_with_element_not_empty)
{
	TSimpleStack st;
	st.Put(1);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimplestack, stack_after_get_operation_not_full)
{
	TSimpleStack st;
	for (int i = 0; i < DefMemSize; i++)
		st.Put(i);

	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TSimplestack, get_operation_take_top_element)
{
	TSimpleStack st;
	st.Put(0);
	st.Put(1);
	st.Put(2);

	EXPECT_EQ(2, st.Get());
}

TEST(TSimplestack, throws_when_put_in_full_stack)
{
	TSimpleStack st;
	for (int i = 0; i < DefMemSize; i++)
		st.Put(i);

	ASSERT_ANY_THROW(st.Put(DefMemSize));
}

TEST(TSimplestack, throws_when_get_from_empty_stack)
{
	TSimpleStack st;

	ASSERT_ANY_THROW(st.Get());
}

TEST(TSimplestack, can_create_copied_stack)
{
	TSimpleStack st;

	ASSERT_NO_THROW(TSimpleStack st1(st));
}

TEST(TSimplestack, copied_stack_is_equal_to_source_one)
{
	bool res = true;
	TSimpleStack st1;
	for (int i = 0; i < DefMemSize; i++)
		st1.Put(i);
	TSimpleStack st2(st1);
	for (int i = 0; i < DefMemSize; i++)
		if (st1.Get() != st2.Get())
			res = false;

	EXPECT_TRUE(res);
}

TEST(TSimplestack, copied_stack_has_its_own_memory)
{
	TSimpleStack st1;
	TSimpleStack st2(st1);

	EXPECT_NE(&st1, &st2);
}
