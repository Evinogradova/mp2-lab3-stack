#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	DataCount = 0;
	MemSize = Size;
	if (Size < 0)
		throw SetRetCode(DataErr);
	else if (Size == 0)
	{
		pMem = nullptr;
		MemType = MEM_RENTER;
	}
	else
	{
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}
}

TDataRoot::~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = nullptr;
}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	for (int i = 0; i < DataCount; i++)
		((PTElem)p)[i] = pMem[i];
	if (MemType == MEM_HOLDER)
		delete[]pMem;
	MemType = MEM_RENTER;
	pMem = (PTElem)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}